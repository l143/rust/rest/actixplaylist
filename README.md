```
ACTIX-API

█▀█ █░░ ▄▀█ █▄█ █░░ █ █▀ ▀█▀
█▀▀ █▄▄ █▀█ ░█░ █▄▄ █ ▄█ ░█░
```

This repo contains examples of actix use and concepts.

All the content is distributes in branches.

## Navigation

```bash
$ git checkout <branch-name>
```

## Branches

- [**main**](https://gitlab.com/l143/rust/rest/actixplaylist/-/tree/main)  
  - Initial project
- [**basic_server**](https://gitlab.com/l143/rust/rest/actixplaylist/-/tree/basic_server)
  - Basic actix server
- [**env**](https://gitlab.com/l143/rust/rest/actixplaylist/-/tree/env)
  - Environment variables
  - Config
  - Structs
- [**Endpoint**](https://gitlab.com/l143/rust/rest/actixplaylist/-/tree/endpoint)
  - Endpoint as function
  - refact endpoint into a module
- [**Json**](https://gitlab.com/l143/rust/rest/actixplaylist/-/tree/json)
  - Module convention
  - Domain Struct
- [**URL query**](https://gitlab.com/l143/rust/rest/actixplaylist/-/tree/url_query)
  - Derive Serialize
  - Use URL query in *GET* request
- [**Post**](https://gitlab.com/l143/rust/rest/actixplaylist/-/tree/post)
  - Derive Deserialize
  - Use Json in *POST* request
- [**State**](https://gitlab.com/l143/rust/rest/actixplaylist/-/tree/state)
  - Define a state
  - Mutex
  - Mutate state